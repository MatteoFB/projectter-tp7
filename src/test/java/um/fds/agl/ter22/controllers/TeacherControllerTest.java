package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.TeacherForm;
import um.fds.agl.ter22.services.TeacherService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private TeacherService teacherService;

    @MockBean
    private TeacherService teacherServiceMock;

    @Captor
    ArgumentCaptor<TeacherForm> teacherFormCaptor;

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    /*
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        List<String> teachersName = new ArrayList<>();
        for(Teacher t: teacherService.getTeachers()){
            teachersName.add(t.getLastName());
        }
        assumingThat(!teachersName.contains("Kermarrec"), () -> {
            MvcResult result = mvc
                    .perform(post("/addTeacher")
                            .param("firstName", "Anne-Marie")
                            .param("lastName", "Kermarrec")
                            //.param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();

        });
        teachersName.clear();
        for(Teacher t: teacherService.getTeachers()){
            teachersName.add(t.getLastName());
        }
        assertTrue(teachersName.contains("Kermarrec"));
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        List<String> teachersName = new ArrayList<>();
        MvcResult result = mvc
                .perform(post("/addTeacher")
                                .param("firstName", "Anne-Marie")
                                .param("lastName", "Kermarrec")
                        //.param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        for(Teacher t: teacherService.getTeachers()){
            teachersName.add(t.getLastName());
        }
        assumingThat(teachersName.contains("Kermarrec"), () -> {
            MvcResult result1 = mvc
                    .perform(post("/addTeacher")
                            .param("firstName", "")
                            .param("lastName", "Kermarr")
                            //.param("id", "10")
                    )
                    .andExpect(status().is3xxRedirection())
                    .andReturn();
        });
        teachersName.clear();
        for(Teacher t: teacherService.getTeachers()){
            teachersName.add(t.getLastName());
            System.out.println(t.getLastName());
        }
        assertTrue(teachersName.contains("Kermarrec"));

    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacherWithMock() throws Exception {

        MvcResult result = mvc
                .perform(post("/addTeacher")
                                .param("firstName", "Anne-Marie")
                                .param("lastName", "Kermarrec")
                        //.param("id", "10")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();

        Mockito.verify(teacherServiceMock).saveTeacher(new Teacher("Kermarr","Anne-Marie", new TERManager()));
        TeacherForm value = teacherFormCaptor.getValue();
        assertTrue(value.getLastName().equals("Kermarr"));

    }
    */
}