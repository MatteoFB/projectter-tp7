package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.UserTER;

import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

public class SubjectForm {

    private String title;
    private String subject;
    private long id;
    private String name;
    @OneToMany
    private List<String> listEncadrant;

    public SubjectForm(long id, String title, String subject,String name,List<String> listEncadrant){
        this();
        this.title=title;
        this.subject=subject;
        this.name=name;
        this.id=id;
        this.listEncadrant= listEncadrant;
    }

    public SubjectForm(){
        this.listEncadrant=new ArrayList<>();
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getListEncadrant() {
        return listEncadrant;
    }
    public void setListEncadrant(List<String> e) {
        this.listEncadrant = e;
    }

    /*public boolean containsEncadrant(UserTER encad){
        return this.listEncadrant.contains(encad);
    }*/

    /*public void addEncadrant(UserTER encad){
        if(!containsEncadrant(encad)) {
            this.listEncadrant.add(encad);
        }
    }

    public void removeEncadrant(UserTER encad){
        if(containsEncadrant(encad)){
            this.listEncadrant.remove(encad);
        }
    }*/
}
