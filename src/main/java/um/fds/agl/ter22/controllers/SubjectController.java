package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.entities.UserTER;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.repositories.UserTERRepository;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeacherService;

import java.lang.reflect.Array;
import java.util.ArrayList;

@Controller
public class SubjectController  implements ErrorController {

    @Autowired
    private UserTERRepository repository;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/listTER")
    public Iterable<Subject> getSubjects(Model model){
        Iterable<Subject> subjects=subjectService.getSubjects();
        model.addAttribute("subjects", subjects);
        return subjects;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value={"/addSubject"})
    public String showAddSubjectPage(Model model,@CurrentSecurityContext(expression="authentication?.name") String name){
        SubjectForm subjectForm = new SubjectForm();
        subjectForm.setName(name);
        model.addAttribute("subjectForm", subjectForm);
        model.addAttribute("teacherList", teacherService.getTeachers());
        return "addSubject";
    }

    @PostMapping(value={"/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm,@CurrentSecurityContext(expression="authentication?.name") String name){
        Subject s;
        if(subjectService.findById(subjectForm.getId()).isPresent()){
            // teacher already existing : update
            s = subjectService.findById(subjectForm.getId()).get();
            s.setTitle(subjectForm.getTitle());
            s.setSubject(subjectForm.getSubject());
            s.setName(name);
            ArrayList<UserTER> U = new ArrayList<>();
            for(String lastName: subjectForm.getListEncadrant()){
                if(repository.findByLastName(lastName)!=null && lastName!=name) {
                    U.add(repository.findByLastName(lastName));
                }
            }
            s.setListEncadrant(U);
        }else {
            ArrayList<UserTER> U = new ArrayList<>();
            //String laListe = subjectForm.getListEncadrant();
            //String[] TabUserString=laListe.split(",");
            for(String lastName: subjectForm.getListEncadrant()){
                if(repository.findByLastName(lastName)!=null) {
                        U.add(repository.findByLastName(lastName));
                }
            }
           // U.add(repository.findByLastName("Turing"));
            s = new Subject(subjectForm.getTitle(), subjectForm.getSubject(), subjectForm.getName(), U);
        }
        subjectService.saveSubject(s);
        return "redirect:/listTER";
    }
    @GetMapping(value = {"/showSubjectUpdateForm/{id}"})
    @PreAuthorize("hasRole('ROLE_MANAGER') or @subjectRepository.findById(#id).get()?.name == authentication?.name")
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id,@CurrentSecurityContext(expression="authentication?.name") String name){
        model.addAttribute("teacherList", teacherService.getTeachers());
        ArrayList<String> li = new ArrayList<>();
        for(Teacher t : teacherService.getTeachers()){
            if(!t.getLastName().equals(name)) {
                li.add(t.getLastName());
            }
        }
        SubjectForm subjectForm = new SubjectForm(id, subjectService.findById(id).get().getTitle(), subjectService.findById(id).get().getSubject(),
                subjectService.findById(id).get().getName(), li);
        model.addAttribute("subjectForm", subjectForm);
        return "updateSubject";
    }

    //@PreAuthorize("hasRole('ROLE_MANAGER') or #teacher?.terManager?.lastName == authentication?.name")
    @GetMapping(value={"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id){
        subjectService.deleteSubject(id);
        return "redirect:/listTER";
    }


}
