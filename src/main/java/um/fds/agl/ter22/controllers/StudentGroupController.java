package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.StudentGroup;
import um.fds.agl.ter22.entities.UserTER;
import um.fds.agl.ter22.forms.StudentGroupForm;
import um.fds.agl.ter22.repositories.UserTERRepository;
import um.fds.agl.ter22.services.StudentGroupService;

@Controller
public class StudentGroupController implements ErrorController {

    @Autowired
    private StudentGroupService studentGroupService;

    @Autowired
    private UserTERRepository repository;

    @GetMapping("/listStudentGroups")
    public Iterable<StudentGroup> getStudentGroups(Model model) {
        Iterable<StudentGroup> studentGroups = studentGroupService.getStudentGroups();
        model.addAttribute("studentGroups", studentGroups);
        return studentGroups;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = { "/addStudentGroup" })
    public String showAddStudentGroupPage(Model model) {

        StudentGroupForm StudentGroupForm = new StudentGroupForm();
        model.addAttribute("studentGroupForm", StudentGroupForm);

        return "addStudentGroup";
    }

    @PostMapping(value = { "/addStudentGroup"})
    public String addStudentGroup(Model model, @ModelAttribute("StudentGroupForm") StudentGroupForm studentGroupForm, @CurrentSecurityContext(expression="authentication?.name") String name) {

        if(studentGroupForm.getName().isBlank()){
            return "redirect:/listStudentGroups";
        }

        StudentGroup studentGroup = null;

        UserTER user = repository.findByLastName(name);
        if(user instanceof Student){
            Student student = (Student)user;
            if(!studentAlreadyInAGroup(student)){
                studentGroup = new StudentGroup(studentGroupForm.getName());
                studentGroup.addStudent((Student)user);
            }
        }else{
            studentGroup = new StudentGroup(studentGroupForm.getName());
        }

        if(studentGroup!=null) {
            studentGroupService.saveStudentGroup(studentGroup);
        }

        return "redirect:/listStudentGroups";

    }

    @GetMapping(value={"/joinStudentGroup/{id}"})
    public String joinStudentGroup(Model model, @PathVariable(value = "id") long id, @CurrentSecurityContext(expression="authentication?.name") String name){
        StudentGroup studentGroup = null;
        if(studentGroupService.findById(id).isPresent()){
            studentGroup = studentGroupService.findById(id).get();

            UserTER user = repository.findByLastName(name);
            if(user instanceof Student){
                Student student = (Student)user;
                if(!studentAlreadyInAGroup(student)){
                    studentGroup.addStudent(student);
                }
            }

        }
        if(studentGroup!=null) {
            studentGroupService.saveStudentGroup(studentGroup);
        }
        return "redirect:/listStudentGroups";
    }

    @GetMapping(value={"/leaveStudentGroup/{id}"})
    public String leaveStudentGroup(Model model, @PathVariable(value = "id") long id, @CurrentSecurityContext(expression="authentication?.name") String name){
        StudentGroup studentGroup = null;
        if(studentGroupService.findById(id).isPresent()){
            studentGroup = studentGroupService.findById(id).get();
            UserTER user = repository.findByLastName(name);
            if(user instanceof Student){
                studentGroup.removeStudent((Student)user);
            }
        }
        if(studentGroup!=null) {
            studentGroupService.saveStudentGroup(studentGroup);
        }

        if(studentGroup.getStudentList().isEmpty()){
            studentGroupService.deleteStudentGroup(id);
        }

        return "redirect:/listStudentGroups";
    }

    private boolean studentAlreadyInAGroup(Student student){
        for(StudentGroup studentGroup : studentGroupService.getStudentGroups()){
            if(studentGroup.containStudent(student)){
                return true;
            }
        }
        return false;
    }


}
