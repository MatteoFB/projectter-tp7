package um.fds.agl.ter22.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Subject {
    private @Id @GeneratedValue Long id;
    //private @OneToOne Teacher teacher;
    private String title;
    private String subject;
    private String name;
    @OneToMany
    private List<UserTER> listEncadrant;
    // private Responsible resp; card 1-1

    public Subject(String title,String subject,String name){
        this.title=title;
        this.subject=subject;
        this.name=name;
    }
    public Subject(String title,String subject,String name,List<UserTER> listEncadrant){
        this.title=title;
        this.subject=subject;
        this.name=name;
        this.listEncadrant= listEncadrant;
    }

    public Subject(Long Id,String title, String subject, String name) {
        this.title=title;
        this.subject=subject;
        this.name=name;
        this.id=Id;

    }
    public Subject(Long Id,String title, String subject, String name,List<UserTER> listEncadrant) {
        this.title=title;
        this.subject=subject;
        this.name=name;
        this.id=Id;
        this.listEncadrant= listEncadrant;
    }
// lalala
    public Subject(){}

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserTER> getListEncadrant() {
        return this.listEncadrant;
    }

    public void setListEncadrant(List<UserTER> le) {
        this.listEncadrant = le;
    }

    public boolean containsEncadrant(UserTER encad){
        return this.listEncadrant.contains(encad);
    }

    public void addEncadrant(UserTER encad){
        if(!containsEncadrant(encad)) {
            this.listEncadrant.add(encad);
        }
    }

    public void removeEncadrant(UserTER encad){
        if(containsEncadrant(encad)){
            this.listEncadrant.remove(encad);
        }
    }


}
