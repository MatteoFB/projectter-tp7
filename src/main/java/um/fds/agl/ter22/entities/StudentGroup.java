package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudentGroup {

    private @Id @GeneratedValue Long id;

    private String name;

    private @OneToMany List<Student> studentList;

    public StudentGroup(){
        this.studentList = new ArrayList<>();
    }

    public StudentGroup(String name){
        this();
        this.name = name;
    }

    public StudentGroup(Long id, String name){
        this(name);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudentList(){
        return this.studentList;
    }

    public void setStudentList(List<Student> studentsId) {
        this.studentList = studentsId;
    }

    public void addStudent(Student student){
        if(student!=null) {
            this.studentList.add(student);
        }
    }

    public void removeStudent(Student student){
        if(this.studentList.contains(student)){
            this.studentList.remove(student);
        }
    }

    public boolean containStudent(Student student){
        return this.studentList.contains(student);
    }
}
